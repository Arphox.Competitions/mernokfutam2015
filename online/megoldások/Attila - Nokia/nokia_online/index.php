<?php 
ob_start();
session_start();
if ( !isset($_SESSION["username"]))
{
header("Location: login.php");
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <script src= "http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <script src="JS/app.js"></script>
        <script src="JS/pageJquery.js"></script>

        <title>Nokia-online feladat</title>

    </head>
    <body>
        <div class="container">
            <div class="jumbotron">
                <h1>CV kereső </h1>
                <p>Keressen egyszerűen</p> 
            </div>

            <div class="row">

                <div ng-app="myApp" ng-controller="customersCtrl"> 
                    <div>
                        <?php echo "Üdvözöljük ".$_SESSION["username"]; ?>
                        </br> 
                        <a href="logout.php"> Kijelentkezés </a>
                        
                    </div>
                    <div>
                        <table>
                            <tr>
                                <td>Kulcsszó:  </td><td><input type="text" ng-model="actualKeyWord.word"></td>
                            </tr>
                            <tr>
                                <td>Fontosság: </td><td><input type="text" ng-model="actualKeyWord.point"></td>
                            </tr>

                        </table>
                        <button ng-click="addElementToKeyWordsList()">Ment</button>
                        <button ng-click="deleteKeyWordsList()">Lista törlés</button>
                        <button ng-click="startSearch()">Keresés</button>
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Kulcsszó</th>
                                <th>Prioritás</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-click="toggle(x)" ng-repeat="user in keyWordsList">
                                <td>{{ user.word}}</td>
                                <td>{{ user.point}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Pontok</th>
                                <th>Link</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-click="toggle(x)" ng-repeat="user in cvs| orderBy:'-points'">
                                <td>{{ user.cvEmails[0]}}</td>
                                <td>{{ user.points}}</td>
                                <td>
                                    <a href="CVS/{{user.fileName}}">{{user.fileName}}</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

                <div class="col-sm-4">
                    <h3>Egyszerű</h3>
                    <p>Csak írja be a kulcsszavakat.</p>
                    <p>Adja meg mennyire érdekes az adott kulcsszó.</p>
                </div>
                <div class="col-sm-4">
                    <h3>Gyors</h3>
                    <p>Szerver oldali keresések</p>
                </div>
                <div class="col-sm-4">
                    <h3>Hatékony</h3>        
                    <p>A pc-n lévő cvkből kiválasztja a legjobbakat.</p>
                </div>
            </div>
        </div>

    </body>
</html>
