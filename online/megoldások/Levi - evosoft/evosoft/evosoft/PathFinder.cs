﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace evosoft
{
    class PathFinder
    {
        Cell[,] map;
        Cell start, finish, tempstart;
        List<Cell> actualPath;
        List<Cell> shortestPath;
        List<Cell> checkpoints;
        List<Teleport> teleports;
        List<Teleport> usedTeleports;
        List<Cell> pathWithTeleports;
        public Cell[,] Map
        {
            get
            {
                return map;
            }
        }

        public Cell Start
        {
            get
            {
                return start;
            }

            set
            {
                start = value;
            }
        }

        public Cell Finish
        {
            get
            {
                return finish;
            }

            set
            {
                finish = value;
            }
        }

        public List<Cell> ActualPath
        {
            get
            {
                return actualPath;
            }
        }

        public List<Cell> ShortestPath
        {
            get
            {
                return shortestPath;
            }
        }

        public PathFinder(List<Cell> cells, int width, int height, Cell start, Cell finish, List<Cell> checkpoint, List<Teleport> teleport)
        {
            actualPath = new List<Cell>();
            shortestPath = new List<Cell>();
            checkpoints = checkpoint;
            teleports = teleport;
            usedTeleports = new List<Teleport>();
            pathWithTeleports = new List<Cell>();

            map = new Cell[width, height];
            foreach (Cell item in cells)
            {
                map[item.X, item.Y] = item;
            }
            this.start = start;
            this.finish = finish;
            tempstart = start;
            foreach (Cell item in cells)
            {
                if (item.Equals(start))
                {
                    start.Value = item.Value;
                }
                if (item.Equals(finish))
                {
                    finish.Value = item.Value;
                }
            }
            actualPath.Add(finish);
            if (teleports == null)
            {
                bool res = false;
                FindPath(finish, ref res);
            }
            else
            {
                bool res = false;
                int timeOut = 10000;
                while (timeOut > 0 && usedTeleports.Count != teleports.Count)
                {
                    foreach (Teleport item in teleports)
                    {
                        if (!usedTeleports.Contains(item))
                        {
                            tempstart = item.Src;                            
                            FindPath(finish, ref res);                           
                            if (res)
                            {
                                usedTeleports.Add(item);
                                ShortestPath.Add(item.Dst);
                                finish = GetCellFromTeleport(item.Dst);
                                pathWithTeleports.AddRange(shortestPath);
                                shortestPath.Clear();
                            }
                            else
                            {
                                tempstart = item.Dst;
                                FindPath(finish, ref res);
                                if (res)
                                {
                                    usedTeleports.Add(item);
                                    ShortestPath.Add(item.Src);
                                    finish = GetCellFromTeleport(item.Src);
                                    pathWithTeleports.AddRange(shortestPath);
                                    shortestPath.Clear();
                                }
                            }
                        }
                    }
                    timeOut--;
                }
                tempstart = start;
                FindPath(finish, ref res);
                pathWithTeleports.AddRange(shortestPath);
                shortestPath = pathWithTeleports;
            }            
        }

        Cell GetCellFromTeleport(Cell teleportCell)
        {
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (teleportCell.Equals(map[i, j]))
                    {
                        return map[i, j];
                    }
                }
            }
            return null;
        }

        public override string ToString()
        {
            string res = String.Empty;
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    res += (map[i, j].Value + " ");
                }
                res += "\n";
            }
            return res;
        }

        //public void Kiir()
        //{
        //    for (int i = 0; i < map.GetLength(0); i++)
        //    {
        //        for (int j = 0; j < map.GetLength(1); j++)
        //        {
        //            if (map[i, j].Value != -1)
        //            {
        //                if (teleports != null && (map[i, j].Equals(teleports.First().Src) || map[i, j].Equals(teleports.First().Dst)))
        //                {
        //                    Console.ForegroundColor = ConsoleColor.Blue;
        //                    Console.Write(map[i, j].Value + " ");
        //                }
        //                else
        //                {
        //                    if (map[i, j].X == start.X && map[i, j].Y == start.Y)
        //                    {
        //                        Console.ForegroundColor = ConsoleColor.Yellow;
        //                    }
        //                    else if (map[i, j].X == finish.X && map[i, j].Y == finish.Y)
        //                    {
        //                        Console.ForegroundColor = ConsoleColor.Red;
        //                    }
        //                    else if (shortestPath.Contains(map[i, j]) || pathWithTeleports.Contains(map[i,j]))
        //                    {
        //                        Console.ForegroundColor = ConsoleColor.Green;
        //                    }
        //                    else
        //                    {
        //                        Console.ForegroundColor = ConsoleColor.Gray;
        //                    }
        //                    Console.Write((map[i, j].Value + " "));
        //                }
        //            }
        //            else
        //            {
        //                Console.Write("  ");
        //            }
        //        }
        //        Console.WriteLine();
        //    }
        //    Console.WriteLine();
        //    Console.ForegroundColor = ConsoleColor.Gray;
        //}

        bool FindPath(Cell actualCell, ref bool res)
        {
            if (actualCell.Equals(tempstart))
            {
                return true;
            }
            else
            {
                if (actualCell.Sources.Count == 0)
                {
                    GetSources(actualCell);
                }
                foreach (Cell item in actualCell.Sources)
                {
                    if (!actualPath.Contains(item))
                    {                        
                        ActualPath.Add(item);
                        bool resu = FindPath(item, ref res);
                        if (resu)
                        {
                            res = true;
                            if (CheckpointsCompleted())
                            {
                                if (ShortestPath.Count == 0 || ActualPath.Count < ShortestPath.Count)
                                {
                                    CopyToShortest();
                                }
                            }
                        }
                        ActualPath.Remove(item);
                    }
                }
                return false;
            }
        }

        void CopyToShortest()
        {
            if (shortestPath.Count != 0)
            {
                shortestPath.Clear();
            }
            foreach (Cell item in ActualPath)
            {
                ShortestPath.Add(item);
            }

        }

        void GetSources(Cell targetCell)
        {
            for (int i = targetCell.Y - 1; i >= 0; i--)
            {
                if (map[targetCell.X, i].Value == targetCell.Y - i && !WallBetween(map[targetCell.X, i], targetCell))
                {                    
                    targetCell.Sources.Add(map[targetCell.X, i]);
                }
            }
            if (targetCell.Y < map.GetLength(1) - 1)
            {
                for (int i = targetCell.Y + 1; i < map.GetLength(1); i++)
                {
                    if (map[targetCell.X, i].Value == i - targetCell.Y && !WallBetween(map[targetCell.X, i], targetCell))
                    {
                        targetCell.Sources.Add(map[targetCell.X, i]);
                    }
                }
            }
            for (int i = targetCell.X - 1; i >= 0; i--)
            {
                if (map[i, targetCell.Y].Value == targetCell.X - i && !WallBetween(map[i, targetCell.Y], targetCell))
                {
                    targetCell.Sources.Add(map[i, targetCell.Y]);
                }
            }
            if (targetCell.X <= map.GetLength(0) - 1)
            {
                for (int i = targetCell.X + 1; i < map.GetLength(0); i++)
                {
                    if (map[i, targetCell.Y].Value == i - targetCell.X && !WallBetween(map[i, targetCell.Y], targetCell))
                    {
                        targetCell.Sources.Add(map[i, targetCell.Y]);
                    }
                }
            }
        }

        bool WallBetween(Cell src, Cell dst)
        {
            if (src.X == dst.X)
            {
                if (src.Y > dst.Y)
                {
                    int i = src.Y;
                    while (i > dst.Y && map[src.X, i].Value != -1)
                    {
                        i--;
                    }
                    return i > dst.Y;
                }
                else
                {
                    int i = dst.Y;
                    while (i > src.Y && map[src.X, i].Value != -1)
                    {
                        i--;
                    }
                    return i > src.Y;
                }
            }
            else
            {
                if (src.X > dst.X)
                {
                    int i = src.X;
                    while (i > dst.X && map[i, src.Y].Value != -1)
                    {
                        i--;
                    }
                    return i > dst.X;
                }
                else
                {
                    int i = dst.X;
                    while (i > src.X && map[i, src.Y].Value != -1)
                    {
                        i--;
                    }
                    return i > src.X;
                }
            }
        }

        bool CheckpointsCompleted()
        {
            if (checkpoints != null)
            {
                foreach (Cell item in checkpoints)
                {
                    if (!actualPath.Contains(item))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
