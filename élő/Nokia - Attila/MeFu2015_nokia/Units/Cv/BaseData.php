<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BaseData
 *
 * @author Matesz
 */
class BaseData {
    public $emails;
    public $phoneNumbers;
    public $foundKeyWords;
    public $path;
    public $name;
    
    public function __construct( $emails
                               , $phoneNumbers
                               , $foundKeyWords
                               , $path
                               , $name = ""
                               )
    {
        $this->emails           = $emails;
        $this->phoneNumbers     = $phoneNumbers;
        $this->foundKeyWords    = $foundKeyWords;
        $this->path             = $path;
        $this->name             = $name;
    }
    
    public function GetName ()
    {
        return $this->name;
    }
    
    public function GetEmails ()
    {
        return $this->emails; 
    }
    
    public function GetFoundKeyWords ()
    {
        return $this->foundKeyWords;
    }
    
    public function GetPah () 
    {
        return $this->path;
    }
    
    public function GetPhoneNumbers () 
    {
        return $this->phoneNumbers;
    }
    
    public function AddPhoneNumber ($number)
    {
        array_push($this->phoneNumbers, $number);
    }
    
    public function AddEmail ($email)
    {
        array_push($this->emails, $email);
    }
}
