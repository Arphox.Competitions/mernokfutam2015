<?php

include_once "./BaseData.php";
include_once "./IDataRequirements.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CvData
 *
 * @author Matesz
 */
class CvData implements IDataRequirements {
    public $cvFolder;
    public $cvName;
    public $baseData;
    
    public function __construct( $cvFolder
                               , $cvName
                               , $emails
                               , $phones
                               , $keywords
                               , $name = ""
                               )
    {
        $this->cvFolder = $cvFolder;
        $this->cvName   = $cvName;
        $this->baseData = new BaseData( $emails
                                      , $phones
                                      , $keywords
                                      , $cvFolder==""?$cvName:$cvFolder."/".$cvName
                                      , $name
                                      );
         
        
    }
    public function GetName () {
        return $this->baseData->GetName ();
    }
    
    public function GetEmails () {
        return $this->baseData->GetEmails ();
    }

    public function GetKeyWords() {
        return $this->baseData->GetFoundKeyWords ();
    }

    public function GetPhoneNumbers() {
        return $this->baseData->GetPhoneNumbers ();
    }

}
