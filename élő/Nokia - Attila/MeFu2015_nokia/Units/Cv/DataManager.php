<?php

include_once './WebActivityManager.php';
include_once './CvDataManager.php';


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataManager
 *
 * @author Matesz
 */
class DataManager {
    //put your code here
    
    public $cvDatas;
    private $keyWords;
    private $locs;
    
    public function __construct($Keywords, $Locs) {
        $this->cvDatas = array ();
        
        $this->keyWords=$Keywords;
        $this->locs = $Locs;
                
        $keywords = array ();  
        
        foreach ($this->keyWords as $keyWord) {
            foreach ($keyWord as $value) {
                array_push($keywords, $value->word);
            }          
        }
        foreach ($this->locs as $loc) {
            array_push($keywords, $loc);
        }
        
        $dir = 'c:/Xampp/htdocs/MeFu2015_nokia';
        
        $arrayWebs = array ();
        
        try{
            $wea = new WebActivityManager ($this->keyWords, $this->locs, $keywords);
            $arrayWebs = $wea->GetCvDatas();
        } catch (Exception $ex) {
             echo $ex->getMessage();
        }
        
        $cvdatamanager = new CvDataManager ($dir, $keywords);
        
        $arrayDocs = array();
        $arrayDocs = $cvdatamanager->GetCvDatas();

        
        $this->cvDatas = array_merge($arrayDocs, $arrayWebs);
        
        echo  json_encode($this->cvDatas);

    }
}


