<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DataMerger
 *
 * @author Matesz
 */
class DataMerger {
    //put your code here
    public function __construct() {
        ;
    }
    
    public function MergeIfNeed ($array1,$array2)
    {
        $mergeNeeded = false;
        foreach ($array1 as $value1) {
            foreach ($array2 as $value2) {
                if ($value1 == $value2)
                {
                    $mergeNeeded = true;
                    break;
                }
            }
            
        }
        
        if ($mergeNeeded)
            return true;
        
        return false;
    }
}
