<?php
include '../Converter/DocxConversion.php';
//include './IConverter.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DocText
 *
 * @author Matesz
 */
class DocText {
    private $docConverter;
    private $path;
    
    public function __construct ($path) 
    {
        $this->path = $path;
    }

    public function GetContent($path) {
        if (isset ($path))
            $this->path = $path;
        
        if (!isset ($this->path))
            throw new Exception ("Definialatlan utvonal", 0, 0);


        $this->docConverter = new DocxConversion($this->path);
        return $this->docConverter->convertToText ();
    }

}
