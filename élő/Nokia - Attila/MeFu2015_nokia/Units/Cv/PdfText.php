<?php
include '../Converter/pdf2text.php';
//include './IConverter.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PdfText
 *
 * @author Matesz
 */
class PdfText {
    
    private $pdfToText;
    
    public function __construct()
    {
        $this->pdfToText = new PDF2Text ();
    }

    public function GetContent($path) {
        $this->pdfToText->setFilename ($path);
        $this->pdfToText->decodePDF ();
        return $this->pdfToText->output ();
    }

}
