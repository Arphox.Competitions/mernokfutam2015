<?php
include_once "./BaseData.php";
include_once "./IDataRequirements.php";
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebData
 *
 * @author Matesz
 */
class WebData implements IDataRequirements{
    
    private $baseData;
    
    public function __construct( $path
                               , $emails
                               , $phones
                               , $keywords
                               , $name = ""
                               )
    {
        $this->baseData = new BaseData( $emails
                                      , $phones
                                      , $keywords
                                      , $path
                                      , $name
                                      );

    }

    public function GetName ()
    {
        return $this->baseData->GetName ();
    }
    
    public function GetEmails() {
        return $this->baseData->GetEmails ();
    }

    public function GetKeyWords() {
        return $this->baseData->GetFoundKeyWords ();
    }

    public function GetPhoneNumbers() {
        return $this->baseData->GetPhoneNumbers ();
    }


//put your code here
}
