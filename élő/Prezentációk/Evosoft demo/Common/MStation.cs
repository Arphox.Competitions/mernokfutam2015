﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Common
{
    public class MStation
    {
        MPoint stationPoint;
        bool isRoadWork;

        public bool IsRoadWork
        {
            get { return isRoadWork; }
            set { isRoadWork = value; }
        }

        public MPoint StationPoint
        {
            get { return stationPoint; }
            set { stationPoint = value; }
        }
        int dwellTime;

        public int DwellTime
        {
            get { return dwellTime; }
            set { dwellTime = value; }
        }

        public MStation(XElement element, List<MPoint> points)
        {
            int pid = int.Parse(element.Descendants("Point_ID").First().Value);
            dwellTime = int.Parse(element.Descendants("Dwell_Time").First().Value);
            stationPoint = points.Single(x => x.Id == pid);
        }

        public MStation(int xcoord, int ycoord)
        {
            stationPoint = new MPoint(xcoord, ycoord);
            dwellTime = 10;
            isRoadWork = true;
        }
    }
}
