﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Map
    {
        List<MPoint> points;
        List<MRoad> roads;
        List<MStation> stations;
        List<MPoint> path;
        MTrain train;
        int currentPointIndex;

        public int CurrentDstPointIndex
        {
            get { return currentPointIndex; }
            set { currentPointIndex = value; }
        }

        public MTrain Train
        {
            get { return train; }
            set { train = value; }
        }
        public List<MRoad> Roads
        {
            get { return roads; }
        }

        public List<MStation> Stations
        {
            get { return stations; }
        }

        public List<MPoint> Path
        {
            get { return path; }
        }

        public List<MPoint> Points
        {
            get { return points; }
        }

        public Map(List<MPoint> p, List<MRoad> r, List<MStation> s, List<MPoint> pa, MTrain t)
        {
            points = p;
            roads = r;
            stations = s;
            path = pa;
            train = t;
        }
    }
}
