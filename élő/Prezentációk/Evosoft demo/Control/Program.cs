﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Common;

namespace Control
{    
    class Program
    {
        static void Main(string[] args)
        {
            XmlProcesser proc = new XmlProcesser("input.xml");
            Server.Map = proc.ProcessXml();
            Server.StartListening();
        }
        
    }
}