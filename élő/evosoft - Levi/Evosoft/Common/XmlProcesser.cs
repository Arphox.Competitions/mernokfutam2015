﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Common
{
    public class XmlProcesser
    {
        XDocument currentDocument;

        public XmlProcesser(string filePath)
        {
            currentDocument = XDocument.Load(filePath);
            List<MPoint> points = currentDocument.Root.Descendants("Point").Select(x => new MPoint(x)).ToList();
            List<MRoad> roads = currentDocument.Root.Descendants("Road").Select(x => new MRoad(x, points)).ToList();
            List<MStation> stations = currentDocument.Root.Descendants("Station").Select(x => new MStation(x, points)).ToList();
            var pathq = currentDocument.Root.Descendants("Path_Point_ID").Select(x => x.Value).ToList();
            List<MPoint> path = new List<MPoint>();
            XElement pathElement = currentDocument.Root.Descendants("Path").First();
            MPoint start = points.Single(x => x.Id == int.Parse(pathElement.Descendants("Start_Point_ID").First().Value));
            path.Add(start);
            foreach (var item in pathq)
            {
                path.Add(points.Single(x => x.Id == int.Parse(item)));
            }
            MPoint end = points.Single(x => x.Id == int.Parse(pathElement.Descendants("End_Point_ID").First().Value));
            path.Add(end);            
            MTrain train = currentDocument.Root.Descendants("Train").Select(x => new MTrain(x)).First();

            Map map = new Map(points, roads, stations, path, train);
            //foreach (MPoint item in path)
            //{
            //    Console.WriteLine(item.Id + " " + item.X + " " + item.Y);
            //}
            //Console.WriteLine(train.Id);
            //Console.WriteLine(train.Speed);
            //Console.ReadLine();
        }

        public Map ProcessXml()
        {
            List<MPoint> points = currentDocument.Root.Descendants("Point").Select(x => new MPoint(x)).ToList();
            List<MRoad> roads = currentDocument.Root.Descendants("Road").Select(x => new MRoad(x, points)).ToList();
            List<MStation> stations = currentDocument.Root.Descendants("Station").Select(x => new MStation(x, points)).ToList();
            var pathq = currentDocument.Root.Descendants("Path_Point_ID").Select(x => x.Value).ToList();
            List<MPoint> path = new List<MPoint>();
            XElement pathElement = currentDocument.Root.Descendants("Path").First();
            MPoint start = points.Single(x => x.Id == int.Parse(pathElement.Descendants("Start_Point_ID").First().Value));
            path.Add(start);
            foreach (var item in pathq)
            {
                path.Add(points.Single(x => x.Id == int.Parse(item)));
            }
            MPoint end = points.Single(x => x.Id == int.Parse(pathElement.Descendants("End_Point_ID").First().Value));
            path.Add(end);
            MTrain train = currentDocument.Root.Descendants("Train").Select(x => new MTrain(x)).First();

            Map map = new Map(points, roads, stations, path, train);
            return map;
        }
    }
}
